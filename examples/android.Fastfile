# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Notify when a beta version is available"
  lane :beta do
    slack(
      message: "A new Android beta version was just released in AppDeliver!",
      slack_url: "https://hooks.slack.com/services/your-slack-url"
    )
  end

  desc "Build, sign and submit the appbundle to Google Play Store"
  lane :production do
    upload_to_play_store(
      package_name: "com.example",
      json_key: "/tmp/playstore-serviceaccount.json",
      aab: "../../build/app/outputs/bundle/release/app.aab",
      skip_upload_metadata: true,
      skip_upload_images: true,
      skip_upload_screenshots: true,
    )

    slack(
      message: "A new Android version was just released in Google Play Store!",
      slack_url: "https://hooks.slack.com/services/your-slack-url"
    )
  end
end

# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://docs.fastlane.tools/actions
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "2.28.3"

default_platform :ios

platform :ios do
  desc "Register new devices (does not work on CI/CD environments)"
  lane :register do
    register_devices(
      devices_file: "./devices.txt",
    )
    match(type: "development", force_for_new_devices: true)
  end

  desc "Submit a new Beta Build to App Deliver"
  lane :beta do
    # for some weird reason we need both profiles installed
    # in order to sign using development profile
    match(type: "appstore", readonly: true)
    match(type: "development", readonly: true)

    gym(
      silent: true,
      configuration: "Release",
      export_options: {
        method: "development",
      },
      include_bitcode: true,
      scheme: "Runner",
      output_name: "app.ipa"
    )

    sh("source ../../../scripts/functions.sh && ios_beta_deliver")

    slack(
      message: "A new iOS beta version was just released! Check your AppDeliver!",
      slack_url: "https://hooks.slack.com/services/your-slack-url"
    )
  end

  desc "Submit a new Beta Build to App Deliver"
  lane :production do
    match(type: "appstore", readonly: true)

    gym(
      silent: true,
      configuration: "Release",
      include_bitcode: true,
      scheme: "Runner",
      output_name: "app.ipa"
    )

    deliver(
      ipa: "app.ipa",
      force: true,
      submit_for_review: true,
      automatic_release: true,
      skip_metadata: true,
      skip_screenshots: true
    )

    slack(
      message: "A new iOS version was just submitted for review!",
      slack_url: "https://hooks.slack.com/services/your-slack-url"
    )
  end

  # You can define as many lanes as you want

  after_all do |lane|
    # This block is called, only if the executed lane was successful
  end

  error do |lane, exception|
    slack(
      message: "A fastlane pipeline failed :'(",
      success: false,
      slack_url: "https://hooks.slack.com/services/your-slack-url"
    )
  end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used
# No personal data is recorded. Learn more at https://github.com/fastlane/enhancer

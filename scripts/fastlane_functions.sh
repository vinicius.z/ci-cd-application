#!/bin/bash

set -e          # stop execution on error
set -o pipefail # propagate the error status to the caller
set +a          # make variables change in the current scope only

function ios_beta_deliver() {
  # distribute to android beta group
  firebase --token "$FIREBASE_TOKEN" --non-interactive \
    appdistribution:distribute \
    --app="$FIREBASE_APP_ID_IOS" \
    --release-notes="Auto-generated beta version in local mode" \
    --groups="beta" \
    ../animo.ipa
}
